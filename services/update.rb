# frozen_string_literal: true

module Services
  # This service handles operations of updates on a given device, for its label and safe flag.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Update
    include Singleton

    # Updates a device if it finds it, and the user has all permissions to update it.
    #
    # @param id [BSON::ObjectId] the ID of the device you want to get informations about
    # @param session [Arkaan::Authentication::Session] the session of the user requesting
    #   the informations of the device
    # @return [Arkaan::Authentication::Device] the device if every check went well
    #
    # @raise [Virtuatable::API::Errors::Forbidden] if any check fails during execution
    # @raise [Virtuatable::API::Errors::Unknown] if the device is not found in the database
    def run(params, session)
      device = Arkaan::Authentication::Device.find(params['id'])
      check_belonging(device, session)
      check_security(session)
      check_self_device(device, session)
      device.update_attributes(sanitize(params))
      device.save!
      device
    end

    private

    # Checks if the device belongs to the user linked to the session. A device belongs
    # to a user if the user has one or more sessions linked to this device.
    #
    # @param device [Arkaan::Authentication::Device] the device to seek in the user's devices
    # @param session [Arkaan::Authentication::Session] the session of the user to check.
    # @return [Arkaan::Authentication::Device] the device if it belongs to the user
    #
    # @raise [Virtuatable::API::Errors::Forbidden] if the device does not belong to the user
    def check_belonging(device, session)
      return device if session.account.sessions.pluck(:device_id).include? device.id

      raise Virtuatable::API::Errors::Forbidden.new(
        field: 'id',
        error: 'forbidden'
      )
    end

    # Checks if the device from which the request is done is considered safe.
    # @param session [Arkaan::Authentication::Session] the session making the request
    # @return [Arkaan::Authentication::Device] the device if it's considered safe
    # @raise [Virtuatable::API::Errors::Forbidden] if the device is not safe
    def check_security(session)
      return if session.device.safe

      raise Virtuatable::API::Errors::Forbidden.new(
        field: 'id',
        error: 'unsafe'
      )
    end

    # Checks if the updated device is not the device from which the request is done
    #
    # @param session [Arkaan::Authentication::Session] the session making the request
    # @param device [Arkaan::Authentication::Device] the device updated by the user
    # @return [Arkaan::Authentication::Device] the device if it's not the requester's one
    #
    # @raise [Virtuatable::API::Errors::Forbidden] if the device is not the requester's one
    def check_self_device(device, session)
      return if device.id != session.device.id

      raise Virtuatable::API::Errors::Forbidden.new(
        field: 'id',
        error: 'current'
      )
    end

    def sanitize(params)
      params.select { |k, _| %w[safe label].include? k }
    end
  end
end
