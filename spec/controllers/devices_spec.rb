# frozen_string_literal: true

RSpec.describe Controllers::Devices do
  def app
    Controllers::Devices
  end

  include_examples 'PUT /:id'
end
