# frozen_string_literal: true

FactoryBot.define do
  factory :empty_device, class: Arkaan::Authentication::Device do
    factory :random_device do
      user_agent { Faker::Internet.user_agent }
      ip { Faker::Internet.unique.ip_v4_address }
    end
  end
end
