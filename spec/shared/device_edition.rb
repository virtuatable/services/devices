# frozen_string_literal: true

RSpec.shared_examples 'PUT /:id' do
  let!(:application) { create(:random_premium_app) }
  let!(:account) { create(:random_administrator) }
  let!(:device) { create(:random_device, safe: true) }
  let!(:session) { create(:random_session, account: account, device: device, duration: 3600) }
  let!(:payload) { { session_id: session.session_id, app_key: application.app_key } }

  it_should_behave_like 'a route', 'put', '/:id'

  # Nominal scenario :
  # - The user makes a request to edit the label and the safe flag of a device
  # - The device belongs to him, and the session he's emitting from is safe
  # - The API edits the device and returns its informations
  describe 'The device is correctly updated' do
    # We can't update the current device, so we link another one to the user.
    let!(:other_device) { create(:random_device) }
    let!(:other_session) { create(:random_session, device: other_device, account: account) }
    let!(:headers) { { 'HTTP_USER_AGENT' => device.user_agent, 'REMOTE_HOST' => device.ip } }
    let!(:browser) { Browser.new other_device.user_agent }

    before do
      params = {
        safe: true,
        label: 'test'
      }
      put "/#{other_device.id}", payload.merge(params), headers
    end
    it 'Returns a 200 (OK) status code' do
      expect(last_response.status).to be 200
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        ip: other_device.ip,
        label: 'test',
        safe: true,
        browser: browser.name,
        platform: browser.platform.name
      )
    end
    describe 'The edited device' do
      let!(:result) { Arkaan::Authentication::Device.find(other_device.id) }

      it 'Has the correct safe flag' do
        expect(result.safe).to be true
      end
      it 'Has the correct label' do
        expect(result.label).to eq 'test'
      end
    end
  end

  # Exception scenario :
  # - The user tries to edit a device, and provides all necessary informations
  # - The device does not exist in the database
  # - The API fails and returns an error status
  describe 'The device linked to the session does not exist' do
    before do
      put '/unknown', payload
    end
    it 'Returns a 404 (Not Found) status code' do
      expect(last_response.status).to be 404
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 404,
        field: 'id',
        error: 'unknown'
      )
    end
  end

  # Exception scenario :
  # - The user tries to edit a device, and provides all necessary informations
  # - The device exists, but is not linked to this user
  # - The API fails and returns an error status
  describe 'The device is not linked to this user' do
    let!(:other_device) { create(:random_device) }

    before do
      put "/#{other_device.id}", payload
    end
    it 'Returns a 403 (Forbidden) status code' do
      expect(last_response.status).to be 403
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 403,
        field: 'id',
        error: 'forbidden'
      )
    end
  end

  # Exception scenario :
  # - The user tries to edit a device, and provides all necessary informations
  # - The device exists, belongs to the user, but has the "safe" flag set to FALSE
  # - The API fails and returns an error status
  describe 'The current session\'s device is not considered safe' do
    let!(:device) { create(:random_device, safe: false) }
    let!(:browser) { Browser.new device.user_agent }
    let!(:headers) { { 'HTTP_USER_AGENT' => device.user_agent, 'REMOTE_HOST' => device.ip } }

    before do
      put "/#{device.id}", payload, headers
    end
    it 'Returns a 403 (Forbidden) status code' do
      expect(last_response.status).to be 403
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 403,
        field: 'id',
        error: 'unsafe'
      )
    end
  end

  # Exception scenario :
  # - The user tries to edit a device, and provides all necessary informations
  # - The device exists, belongs to the user, is safe, but it's the device from
  #   which the request is made on the API
  # - The API fails and returns an error status
  describe 'The user is trying to edit its current device' do
    let!(:headers) { { 'HTTP_USER_AGENT' => device.user_agent, 'REMOTE_ADDR' => device.ip } }

    before do
      put "/#{device.id}", payload, headers
    end
    it 'Returns a 403 (Forbidden) status code' do
      expect(last_response.status).to be 403
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 403,
        field: 'id',
        error: 'current'
      )
    end
  end
end
