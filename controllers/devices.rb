# frozen_string_literal: true

# Main module for all the controllers of the web application
# @author Vincent Courtois <courtois.vincent@outlook.com>
module Controllers
  # Insert more informations about your controller here.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Devices < Virtuatable::Controllers::Base
    api_route 'put', '/:id', options: { premium: true } do
      api_item Services::Update.instance.run(params, session)
    end
  end
end
