# frozen_string_literal: true

require 'bundler'
Bundler.require :runtime

Virtuatable::Application.load!('devices')

run Controllers::Devices
